## Code Issues:

1. reading-list reducer had no implementations for [failedAddToReadingList] or [failedRemoveFromReadingList]
2. Inconsistencies in reducer and action implementations. Some places use bookID, others ID, some use action.item, others action.book. Consistent pattern implementations make application maintenance more predictable
3. There are a few scattered items that don't have types defined. Consider adding these for better IDE support with type properties
